<?php
namespace org;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/22
 * Time: 17:02
 */
class RepaymentCalendar
{
    var $totalMoney;//固定额度
    var $availableMoney;//可用额度
    var $billDay;//账单日
    var $finallyRepaymentDay;//最后还款日
    var $repaymentMoney;//总还款额
    var $repaymentCount;//总还款次数

    var $calendar = array();//所有日期
    var $weekends = array();//所有周六周日（只消费）
    var $repaymentDays = array();//所有还款日
    var $workDays = array();//所有工作日

    /**
     * 总还款次数
     * @param $totalMoney
     * @param $availableMoney
     */
    public function getRepaymentCount($totalMoney,$availableMoney)
    {
        $this->repaymentCount = ceil((float)$totalMoney/$availableMoney);
    }

    /**
     * 总还款额
     * @param $totalMoney
     */
    public function getTotalRepaymentMoney($totalMoney)
    {
        $percent = ceil((float)((rand(10,30)+100)/100));
        $this->repaymentMoney = $percent * $totalMoney;
    }

    /**
     * 获取休息日
     * @param $calendar
     */
    public function getWeekends($calendar){
        $length = is_array($calendar) ? count($calendar) : false ;
        for($i = 0 ; $i < $length ; $i ++) {
            if((date('w',strtotime($calendar[$i])) == 6) || (date('w',strtotime($calendar[$i])) == 0)) {
                array_push($this->weekends,$calendar[$i]);
            }
        }
    }

    /**
     * 获取工作日
     * @param $calendar
     * @param $weekends
     * @return array
     */
    public function getWorkDays($calendar,$weekends)
    {
        $this->workDays = array_diff($calendar,$weekends);
        return $this->workDays;
    }


    /**
     * 所有日期
     * @param  Date  $startdate 开始日期
     * @param  Date  $enddate   结束日期
     * @return Array
     */
    function getCalendar($startdate, $enddate){
        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);
        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;
        // 保存每天日期
        $date = array();
        for($i=0; $i<$days; $i++){
            $date[] = date('Y-m-d', $stimestamp+(86400*$i));
        }
        $this->calendar = $date;
    }

}