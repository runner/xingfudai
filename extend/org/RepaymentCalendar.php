<?php
namespace org;
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/1/22
 * Time: 17:02
 */
class RepaymentCalendar
{
    const DAY = 20;
    public $totalMoney;//固定额度
    public $availableMoney;//可用额度
    public $billDay;//账单日
    public $finallyRepaymentDay;//最后还款日
    public $repaymentMoney;//总还款额
    public $repaymentCount;//总还款次数

    public $calendar = array();//所有日期
    public $weekends = array();//所有周六周日（只消费）
    public $repaymentDays = array();//所有还款日
    public $workDays = array();//所有工作日


    public function __construct($options)
    {
        $this->totalMoney = $options['totalMoney'];
        $this->availableMoney = $options['availableMoney'];
        $this->billDay = $options['billDay'];
        $this->finallyRepaymentDay = $this->billEndDate($options['billDay']);
        $this->calendar = $this->getCalendar($this->billDay,$this->finallyRepaymentDay);
        $this->weekends = $this->getWeekends($this->calendar);
        $this->workDays = $this->getWorkDays($this->calendar,$this->weekends);
        $this->repaymentCount = $this->getRepaymentCount($this->totalMoney,$this->availableMoney);

    }


    /**
     * 总还款次数
     * @param $totalMoney
     * @param $availableMoney
     * @return mixed
     */
    public function getRepaymentCount($totalMoney,$availableMoney)
    {
        $this->repaymentCount = ceil((float)$totalMoney/$availableMoney) + rand(0,1);
        return $this->repaymentCount;
    }

    /**
     * 总还款额
     * @param $totalMoney
     * @return float
     */
    public function getTotalRepaymentMoney($totalMoney)
    {
        $percent = (float)((rand(10,30)+100)/100);
        $this->repaymentMoney = $percent * $totalMoney;
        return $this->repaymentMoney;
    }

    /**
     * 获取休息日
     * @param $calendar
     * @return mixed
     */
    public function getWeekends($calendar){
        $length = is_array($calendar) ? count($calendar) : false ;
        for($i = 0 ; $i < $length ; $i ++) {
            if((date('w',strtotime($calendar[$i])) == 6) || (date('w',strtotime($calendar[$i])) == 0)) {
                array_push($this->weekends,$calendar[$i]);
            }
        }
        return $this->weekends;
    }

    /**
     * 获取工作日
     * @param $calendar
     * @param $weekends
     * @return array
     */
    public function getWorkDays($calendar,$weekends)
    {
        $this->workDays = array_diff($calendar,$weekends);
        return $this->workDays;
    }


    /**
     * 所有日期
     * @param  Date  $startdate 开始日期
     * @param  Date  $enddate   结束日期
     * @return Array
     */
    function getCalendar($startdate, $enddate){
        $stimestamp = strtotime($startdate);
        $etimestamp = strtotime($enddate);
        // 计算日期段内有多少天
        $days = ($etimestamp-$stimestamp)/86400+1;
        // 保存每天日期
        $date = array();
        for($i=0; $i<$days; $i++){
            $date[] = date('Y-m-d', $stimestamp+(86400*$i));
        }
        $this->calendar = $date;
        return $this->calendar;
    }

    /**
     * 最后还款日
     * @param $finallyRepaymentDay
     * @return false|string
     */
    public function billEndDate($finallyRepaymentDay){
        $this->finallyRepaymentDay = date('Y-m-d',strtotime('+20 day',strtotime($finallyRepaymentDay)));
        return $this->finallyRepaymentDay;
    }

    public function getRepaymentDays()
    {
        $length = count($this->workDays);
        $frequence = floor($length/$this->getRepaymentCount($this->totalMoney,$this->availableMoney));
        $workday = array();

        foreach ($this->workDays as $k=>$v){
            array_push($workday,$v);
        }



        return $this->repaymentDays;
    }


}