<?php
namespace app\admin\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        'account'         => 'require|unique:account',
        'password'         => 'confirm:confirm_password',
        'confirm_password' => 'confirm:password',
        'mobile'           => 'number|length:11',
        'email'            => 'email',
        'status'           => 'require',
        'emp_no'           => 'require',
        'dept_id'          => 'require',
        'name'             => 'require'
    ];

    protected $message = [
        'account.require'         => '请输入用户名',
        'account.unique'          => '用户名已存在',
        'password.confirm'         => '两次输入密码不一致',
        'confirm_password.confirm' => '两次输入密码不一致',
        'mobile.require'           => '请输入手机号码',
        'mobile.number'            => '手机号格式错误',
        'mobile.length'            => '手机号长度错误',
        'email.email'              => '邮箱格式错误',
        'status.require'           => '请选择状态',
        'emp_no.require'           => '请输入员工工号',
        'dept_id.require'          => '请选择所属部门',
        'name.require'             => '请输入员工姓名'
    ];
}