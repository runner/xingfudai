<?php
namespace app\index\controller;

use app\common\controller\HomeBase;
use org\RepaymentCalendar;

class Index extends HomeBase
{
    public function index()
    {
        return $this->fetch();
    }

    public function test()
    {
        $options = array(
            'totalMoney' => 10000,
            'availableMoney' => 2000,
            'billDay' => '2017-01-06'
        );
        $repayment = new RepaymentCalendar($options);
        $start_date = $options['billDay'];
        $end_date = $repayment->billEndDate($options['billDay']);
        $all_date = $repayment->getCalendar($start_date,$end_date);
        $weekends = $repayment->getWeekends($all_date);
        $workday = $repayment->getWorkDays($all_date,$weekends);
        $repayment_count =  $repayment->getRepaymentCount($options['totalMoney'],$options['availableMoney']);
        $repaymentMoney = $repayment->getTotalRepaymentMoney($options['totalMoney']);
        var_dump($repayment->getRepaymentDays());
        echo $repayment_count;






        $this->assign('start_date',$start_date);
        $this->assign('end_date',$end_date);
        $this->assign('totalMoney',$options['totalMoney']);
        $this->assign('availableMoney',$options['availableMoney']);
        $this->assign('repaymentMoney',$repaymentMoney);
        $this->assign('repayment_count',$repayment_count);

        //return $this->fetch();
    }

}
