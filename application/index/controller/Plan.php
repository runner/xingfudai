<?php
namespace app\index\controller;

use app\common\controller\HomeBase;
use think\Db;
use think\Request;

class Plan extends HomeBase
{
    public function index()
    {
        return $this->fetch();
    }

    public function collection_plan()
    {
        return $this->fetch();
    }

    public function repayment()
    {
        return $this->fetch();
    }

    public function repayment_detail()
    {
        return $this->fetch();
    }

    public function increase_user()
    {
        return $this->fetch();
    }

    public function joined_user()
    {
        return $this->fetch();
    }

}
