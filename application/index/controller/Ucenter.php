<?php
namespace app\index\controller;

use app\common\controller\HomeBase;
use think\Db;
use think\Request;

class Ucenter extends HomeBase
{
    public function index()
    {
        return $this->fetch();
    }

    public function change_password()
    {
        return $this->fetch();
    }

    public function save_password()
    {

    }

    public function feedback()
    {
        return $this->fetch();
    }

    public function save_feedback()
    {

    }

    public function qrcode()
    {
        return $this->fetch();
    }



}
